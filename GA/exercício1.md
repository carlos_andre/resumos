$$u=(2,-1,2)$$
$$v=(1,2,-2)$$
$$w=au + bw$$
Além disso, w e v são ortogonais, ou seja:
$$v\cdot w = 0$$

Temos portanto:
$$v\cdot (au + bw) = 0$$
$$avu + bvw = 0$$
$$a(vu)+b(vw) = 0$$
$$avu+bvw= 0$$
$$bvw=avu$$