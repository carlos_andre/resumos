# Vetores

## O que são?
Vetores são segmentos de reta que possuem módulo, direção e sentido.
 - Modulo: Tamanho do vetor;
 - Direção: Horizontal e Vertical;
 - Sentido: Ângulo de um vetor.

 ## Representações de um vetor

 Vetores são comumente representados dentro de um plano cartesiano, onde são desenhados como setas, no entanto existem algumas representações matemáticas possíveis:

 $$\overrightarrow{A} = (x,y)$$
 
 Ou ainda:

 $$\overrightarrow{A} = xi + yj$$

 > Nessa representação, por convenção, utilizaremos î para indicar qual o tamanho desse vetor com relação ao eixo x, ĵ para indicar no eixo y e $\text{ \^{k} }$ para indicar no eixo z.

 ## Módulo de um vetor

 Dado um vetor $\overrightarrow{A}=(x,y,z)$, podemos descobrir o módulo desse vetor($|\overrightarrow{A}|$) da seguinte maneira:

 $$|\overrightarrow{A}| = \sqrt{x^2 + y^2}$$
 $$ |\overrightarrow{A}| = \sqrt{x^2 + y^2 + z^2}$$