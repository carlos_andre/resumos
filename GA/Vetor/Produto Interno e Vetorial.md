# Produto Interno e Vetorial

## Produto interno

O produto interno de um vetor é dado de maneira que se $\overrightarrow{A} = ai + bj + ck$ e $\overrightarrow{B} = di + fj + gk$ será um valor real, conforme abaixo:

$$\overrightarrow{A}\times\overrightarrow{B} = a\cdot d + b\cdot f + c\cdot g $$