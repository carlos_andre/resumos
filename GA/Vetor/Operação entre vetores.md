# Operações

 Para todas as operações abaixo vamos considerar:
 
 $$\overrightarrow{A} = ai + bj + ck$$
 $$\overrightarrow{B} = di + fj + gk$$
 $$ t \in \R$$

## Soma e subtração de Vetores

Para somar ou subtrair vetores, podemos simplesmente somar suas componentes em um novo vetor. 

$$\overrightarrow{A} \pm \overrightarrow{B} = (a+d)i \pm (a+d)j \pm (a+d)k$$

## Multiplicação de um Número real por um vetor

Aqui vamos só multiplicar real $t$ pelos componentes do vetor.

$$t\cdot\overrightarrow{A} = (t\cdot a)i + (t\cdot b)j + (t\cdot c)k$$

## Vetor unitário

...