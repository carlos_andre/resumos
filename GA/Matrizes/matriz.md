# Matrizes

## Definição

Uma matriz pode ser entendida como uma tabela que contém valores, organizados como linhas e colunas.
Geralmente, escreve-se o nome de uma matriz com letra maiúscula e representa-se a matriz da seguinte forma:

$$A_{(l,c)}$$

Onde $l$ é o número de linhas e $c$ o número de colunas.

## Soma de matrizes

Deve-se somar as matrizes de forma que cada elemento seja somado ao seu elemento correspondente na outra matriz, assim, as duas matrizes devem ter o mesmo tamanho para serem somadas.

$$\begin{bmatrix} a & b \\ c & d \end{bmatrix} + \begin{bmatrix} e & f \\ g & h \end{bmatrix}=\begin{bmatrix} a+e & b+f \\ c+g & d+h \end{bmatrix}$$

## Multiplicação de uma matriz por um número

Basta multiplicar cada elemento da matriz pelo número.
Assuma, então, que $n\in R$ e $A_{(2,2)}$.

$$n\cdot \begin{bmatrix} a & b \\ c & d \end{bmatrix} = \begin{bmatrix} n\cdot a & n\cdot b \\ n\cdot c & n\cdot d \end{bmatrix}$$

## Multiplicação de matrizes

Ao multiplicar duas matrizes $A_{(2,2)}$ e $B_{(2,3)}$, o resultado será uma nova matriz $AxB_{(2,3)}$. 

Assim, o valor de $A_{ij}$ é dado pela somatória da multiplicação dos termos das linhas, pelos termos das colunas. Conforme matriz abaixo:

$$\begin{bmatrix} a & b \\ c & d \end{bmatrix} + \begin{bmatrix} e & f & g \\ h & i & j \end{bmatrix}=\begin{bmatrix} a*e+b*h & a*f+b*i & a*g+b*h \\ c*e+d*h & c*f+d*i & c*g+d*h \end{bmatrix}$$

## Matriz transposta

Nesse caso, basta transformar as linhas em colunas. 
Segue Exemplo:

$$A=\begin{bmatrix} a & b \\ c & d \end{bmatrix}$$
$$A^T=\begin{bmatrix} a & c \\ b & d \end{bmatrix}$$

#Matriz identidade

Matriz onde todos os elementos são 0, menos os da diagonal principal.

$$I=\begin{bmatrix} 1 & 0 & 0 \\ 0 & 1 & 0 \\ 0 & 0 & 1 \end{bmatrix}$$.

> <b>Importante:</b> Uma matriz identidade, obrigatoriamente é uma matriz quadrada.

## Matriz Inversa

A Matriz inversa de $A$, é definida como aquela que ao ser multiplicada por $A$ tem como resultado uma matriz identidade.

$$A=\begin{bmatrix} 3 & 5 \\ 2 & 7 \end{bmatrix}$$

$$A^{-1}\rightarrow A\cdot A^{-1} = I$$

Sendo $I$ uma matriz identidade e $A$ uma matriz quadrada.

Assim, para a matriz $A$ definida acima temos: 

$$A=\begin{bmatrix} 3 & 5 \\ 2 & 7 \end{bmatrix}\cdot\begin{bmatrix} a & b \\ c & d \end{bmatrix} = \begin{bmatrix} 1 & 0  \\ 0 & 1 \end{bmatrix}$$
<br/>

$$\begin{bmatrix} 3\cdot a+5\cdot c & 3\cdot b+5\cdot d \\ 2\cdot a+7\cdot c & 2\cdot b+7\cdot d \end{bmatrix} = \begin{bmatrix} 1 & 0  \\ 0 & 1 \end{bmatrix}$$

Da multiplicação acima, temos os seguintea sistemas:

$$\begin{cases}3\cdot a+5\cdot c = 1 \\ 2\cdot a+7\cdot c = 0 \end{cases}$$
<br/>

$$\begin{cases}3\cdot b+5\cdot d = 0 \\ 2\cdot b+7\cdot d = 1 \end{cases}$$

Basta, portanto, resolver o sistema.


