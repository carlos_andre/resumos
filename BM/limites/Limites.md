# Definição formal de limites


## O que é limite?

### Conceitos importantes:
Valor absoluto -> Geometricamente, o módulo de um número é a distância entre dois pontos em uma reta.

### Analisando o Limite

Seja $f$ uma função definida em um intervalo aberto qualquer que contenha $a$, excluindo o valor $a$. A afirmação $\lim\limits_{x\rightarrow1} = L$ significa que, para todo número $\varepsilon > 0$, existe um número $\delta > 0$ tal que $|f(x)-L| <\varepsilon$ sem que $0 < |x-a|<\delta$.

# Propriedades de limites

Suponha que $\lim\limits_{x\rightarrow a}f(x)$ e $\lim\limits_{x\rightarrow a}g(x)$ existam e $c$ é um número real qualquer, então:

## Soma e Subtração

> $$\lim\limits_{x\rightarrow a}[f(x)\pm g(x)] = \lim\limits_{x\rightarrow a}f(x) \pm \lim\limits_{x\rightarrow a}g(x) $$

## Multiplicação

> $$\lim\limits_{x\rightarrow a}[c\cdot f(x)] = c\cdot\lim\limits_{x\rightarrow a}f(x) $$
---
>  $$\lim\limits_{x\rightarrow a}[f(x)\cdot g(x)] = \lim\limits_{x\rightarrow a}f(x) \cdot \lim\limits_{x\rightarrow a}g(x) $$
---
>  $$\lim\limits_{x\rightarrow a}\frac{f(x)}{g(x)} = \frac{\lim\limits_{x\rightarrow a}f(x)}{\lim\limits_{x\rightarrow a}g(x)} $$ 
> Atenção: $\lim\limits_{x\rightarrow a}g(x)$ deve ser diferente de 0

## Potência

>  $$\lim\limits_{x\rightarrow a}f(x)^n =\big[\lim\limits_{x\rightarrow a}f(x)\big]^n $$

## Raiz
Se $\lim\limits_{x\rightarrow a}f(x)> 0$, então $n\in Z$ e $n>0$:

>  $$\lim\limits_{x\rightarrow a}\sqrt[n]{f(x)} = \sqrt[n]{\lim\limits_{x\rightarrow a}f(x)}$$

*Se $\lim\limits_{x\rightarrow a}f(x)\leq0$, então $n\in Z$ e $n>0$ e Impar*:

## Módulo

> $$\lim\limits_{x\rightarrow a}|{f(x)}| = \big|{\lim\limits_{x\rightarrow a}f(x)}\big|$$


## Outras relações

> $$\lim\limits_{x\rightarrow a}c=c$$
---
> $$\lim\limits_{x\rightarrow a}x=a$$
