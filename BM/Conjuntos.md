# Conjuntos

## Pertence($\in$)
Quando um elemento x está dentro de um conjunto A, dizemos que $x \in A$

## Não Pertence($\nin$)
Quando um elemento x não está dentro de um conjunto A, dizemos que $x \notin A$

# Operações de conjuntos
Dados os conjuntos A e B

## União($\cup$)
Chamamos de $A\cup B$
$A\cup B = (\forall x(x\in A \or x\in B))$

## Intersecção 
Pega apenas os itens que estão, obrigatóriamente, nos dois conjuntos.

