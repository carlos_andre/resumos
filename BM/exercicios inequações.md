$\tt $
# Exercício

 ## Resolva : $\sqrt{x+2}-\sqrt{3-x} > 1$

 Primeiro vamos definir o domínio:
 > $$x+2 \geq 0$$
 > $$x \geq -2$$
 Assim, o domínio será $\{x\in R\mid x>-2\}$
 
----
 > $$3-x \geq 0$$
 > $$(-1)*3-x \geq 0*(-1)$$
 > $$-3+x \leq 0$$
 > $$x \leq 3$$

 Verficando a segunda condição de existência, é possível concluir que o Domínio é tal que: $\{x \in R \mid -2 \leq x \leq 3\}$

$$ \sqrt{x+2} - \sqrt{3-x} > 1 $$
$$ (\sqrt{x+2} - \sqrt{3-x})^2 > 1^2$$
$$ 5-2\sqrt{-x^2+x+6} > 1$$
$$ -2\sqrt{-x^2+x+6} > -4$$
$$ \sqrt{-x^2+x+6} < 2 $$
$$ \sqrt{-x^2+x+6}^2 < 2^2 $$
$$ -x^2+x+6 < 4 $$
$$ -x^2+x+2 < 0 $$
$$ x < 0 \leftrightarrow {\forall x}\{x \in R \mid -2 \leq x < -1  \text{ ou } 2 < x \leq 3 \} $$ 
$$ x = \{[-2,-1[,]2,3]\}$$


> ## 1. $(a-b)^2 = a^2-2ab+b^2$
> ## 2. $(x+2)(3-x) = -x^2+x+6$
> ## 3. $\frac{-b\pm\sqrt{b^2-4ac}}{2a}$

