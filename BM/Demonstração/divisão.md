# Demonstração para o projeto RW
Demonstre que se $b|a$, então $b|(a-b)$.

$$b|a\rightarrow b|(a-b)$$


(Contrapositiva):
$$\lnot b|(a-b) \rightarrow \lnot b|a$$

Assim, se $\lnot b|(a-b)$ isso significa que, $\forall k\in Z$ tal que $a-b\neq b\cdot k$. E da mesma forma, $\forall k\in Z$ tal que $b\neq b\cdot k$.


