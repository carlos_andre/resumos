# Logaritmo 

## Definições

Definido, em geral por: $\log_xY$;
Onde x é o valor da base e Y o logaritmando.

Podemos entender o $\log_ab = x$, como $a^x=b$.

# Propriedades

## Do Produto
$\log_x(a * b) = \log_xa + \log_xb$

---
## Do quociente
$\log_x(\frac{a}{b}) = \log_xa - \log_xb$

---
## Do expoente
$\log_xa^b = b\log_xa$

---
## Mudança de Base
## $\log_xa=\frac{\log_ya}{\log_yx}$
 
Sendo, y a nova base que desejamos pro nosso logaritmo.

---
## Implicações das propriedades

$\log_a1=0$  
$\log_aa=1$  
$\log_aa^m=m$  
$a^{\log_ab}=b$  

