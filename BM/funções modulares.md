# Função Modular

Denominamos função modular, toda função que possui um módulo em sua lei de formação.  

Exemplos:  
$f(x) = |x+1|$  
$f(x) = |x|^2 + 2|x| - 15$  

Embora não seja comum, uma função modular **pode** possuir uma parte negativa. Segue um exemplo abaixo:  
$f(x) = |x| - 1$  
~~Inserir Imagem~~


Para resolver uma função modular, precisamos primeiro resolver o módulo.
$f(x)=|x+1|$  
 
$|x+1|=\begin{cases} x+1&,\text{se }x+1\geq0\\ -x-1&,\text{se } x+1 < 0 \end{cases}$

Dessa forma, podemos considerar para tal função, o gráfico abaixo:  
~inserir imagem~  



# Equações Modulares

Para resolver equações modulares, o primeiro passo é aplicar a definição de módulo.

### Exemplo:
|x-2|=|3-2x|

Primera condição
$x-2\geq0 \rightarrow x\geq2$  
 
$|x-2| = \begin{cases}x-2 &,\text{x }\geq 2 \\-x+2 &,\text{x }<2 \end{cases}$  

Primera condição
$3-2x\geq0 \rightarrow x\geq-\frac{3}{2}$  
 
$|3-2x| = \begin{cases}3-2x &,\text{x }\leq \frac{3}{2} \\-3+2x &,\text{x }>\frac{3}{2} \end{cases}$

Assim, usando a reta real teremos as seguintes opções de equações:

1. Para $x < \frac{3}{2}$ : $-x+2=-3+2x$  
> $$-x+2=-3+2x \rightarrow 3x = 5$$  
> $$x=\frac{5}{3}$$  
2. Para $x \geq \frac{3}{2}$ e $x<2$ : $-x+2=3-2x$  
> $$-x+2=3-2x \rightarrow x = 1$$  
> $$x=1$$  
3. Para $x>2$: $x-2=3-2x$  
> $$x-2=3-2x \rightarrow 3x = 5$$  
> $$x=\frac{5}{3}$$  

Assim, o conjunto solução é dado por:
$$x=\{\frac{5}{3},1\}$$