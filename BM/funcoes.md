# Funções

## Definção
 Uma função define uma relação entre dois conjuntos, dadas na seguinte forma: $f:R$

$$f(x) = 2x+4$$

$$f^{-1}(x) = \frac{x-4}{2}$$
$$
# $\{R}$

$sqrt(2+x)$