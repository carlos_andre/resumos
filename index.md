# Bem vindo ao resumão de FUV

Aqui está a ordem recomendada para ler os arquivos.

## Derivadas  
[O que são derivadas?](FUV/Derivadas/derivadas.html)  
[Mínimos e Máximos](FUV/Derivadas/minimos_e_maximos.html)  
[Taxas Relacionadas](FUV/Derivadas/taxas_relacionadas.html)  
[Regra de LHopital](FUV/Derivadas/regra_lhopital.html)  
[Polinômios de Taylor](FUV/Derivadas/polinomios_de_taylor.html)  

## Integrais
[O que são integrais?](FUV/Integral/integral.html)  
[Integral por Substituição](FUV/Integral/metodos_de_integracao_-_substituicao.html)  
[Integral por Partes](FUV/Integral/metodos_de_integracao_-_partes.html)  
[Aplicações de Integrais](FUV/Integral/aplicacoes.html)  
