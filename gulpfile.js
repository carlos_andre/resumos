const gulp = require("gulp"),
    markdown = require('gulp-markdownit'),
    rename = require("gulp-rename"),
    wrapper = require('gulp-wrapper'),
    kt = require("katex"),
    tm = require('markdown-it-texmath').use(kt),
    del = require("del");
const configTex = {
    html: true,
    plugins: tm,
    delimiters: 'dollars'
}
var headerHTML = '<!DOCTYPE html>\n' + 
'<html lang="pt-br">\n' + 
'<head>\n' + 
    '<meta charset="UTF-8">\n'  + 
    '<meta name="viewport" content="width=device-width, initial-scale=1.0">\n' + 
    '<meta http-equiv="X-UA-Compatible" content="ie=edge">\n' + 
    '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/github-markdown-css/2.4.1/github-markdown.min.css">\n' +
    '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/highlight.js/9.11.0/styles/default.min.css">\n' +
    '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/KaTeX/0.8.3/katex.min.css">\n' + 
    '<link rel="stylesheet" href="https://gitcdn.xyz/repo/goessner/mdmath/master/css/texmath.css">\n' +
    '<link rel="stylesheet" href="https://gitcdn.xyz/repo/goessner/mdmath/master/css/vscode-texmath.css">\n' +
'</head>' + 
'<body class="markdown-body">\n<div style="width:100%">'

var footerHTML = '</div>\n</body>\n</html>'



function limpar(){
    return del("dist/**");
}

function toHtml(){
    return gulp.src(["index.md",'**/*.md',
        "!node_modules/**"])
            .pipe(markdown(configTex))
            .pipe(wrapper({
                header: headerHTML,
                footer: footerHTML
            }))
            .pipe(rename(function(path){
                path.basename = removerAcentos(path.basename.replace(/[\s]/gi,"_").toLowerCase())
                path.extname = ".html"
            }))
            .pipe(gulp.dest('dist'))

}
function copyImgs(){
    return gulp.src(['**/*.{png,gif,jpg}',
        "!node_modules/**"]).
            pipe(gulp.dest('dist'))

}

function removerAcentos( newStringComAcento ) {
    var string = newStringComAcento;
      var mapaAcentosHex 	= {
          a : /[\xE0-\xE6]/g,
          e : /[\xE8-\xEB]/g,
          i : /[\xEC-\xEF]/g,
          o : /[\xF2-\xF6]/g,
          u : /[\xF9-\xFC]/g,
          c : /\xE7/g,
          n : /\xF1/g
      };
  
      for ( var letra in mapaAcentosHex ) {
          var expressaoRegular = mapaAcentosHex[letra];
          string = string.replace( expressaoRegular, letra );
      }
  
      return string;
}

exports.limpar = limpar;
exports.toHtml = toHtml;
exports.copyImgs = copyImgs;

gulp.task('default', gulp.series(limpar,gulp.parallel(toHtml,copyImgs)));