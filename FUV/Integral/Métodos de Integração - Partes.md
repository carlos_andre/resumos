# Integração por partes

## Conceito

Vamos relembrar inicialmente a regra da multiplicação das derivadas.

Primeiro, assuma as funções $f(x),g(x) \in R$.
Agora, temos que:

$$(f(x)\cdot g(x))' = f'(x)\cdot g(x) + f(x)\cdot g'(x)$$

Assim, podemos integrar os dois lados da equação, teremos:

$$\int (f(x)\cdot g(x))' = \int f'(x)\cdot g(x) + \int f(x)\cdot g'(x)$$

Isolamos o termo $\int f(x)\cdot g'(x)$, enfim temos a fórmula para integração por partes:

$$\int f(x)\cdot g'(x) =f(x)\cdot g(x) - \int f'(x)\cdot g(x)$$

## Utilização

Esse método deve ser utilizado quando tiver uma multiplicação de funções de forma a gerar uma integral mais simples ou que possa ser alcançada por algum método conhecido.

A técnica de integração por partes consiste, basicamente em escolher corretamente as funções para $f(x)$ e $g(x)$. De forma a facilitar a escrita da fórmula, escrevemos $u=f(x)$ e $du=f'(x)$, e também, $v=g(x)$ e $dv=g'(x)$.

<big> $\boxed{\int udv = uv - \int vdu}$</big>

## Exemplo 1

$$\int xe^xdx$$

Não sabemos essa integral, mas sabemos a integral de $e^x$.

Então, vamos chamar $u=x$ e $dv=e^x$, assim teremos $du=1dx$ e $v=e^x$.

Substituindo na fórmula:

$\int xe^xdx = xe^x - \int e^x \cdot 1dx$

$\int xe^xdx = xe^x - e^x$

Simplificando:

$\int xe^xdx = e^x(x-1) + c$

## Exemplo 2

$$\int xe^{\frac{x}{7}}dx$$

Repetiremos o processo acima, da seguinte forma:
$u=x$ e $dv=e^{x/7}$, assim teremos $du=1dx$ e precisamos integrar $dv$.

Usaremos para isso o método da substituição, então $v = \int e^{x/7}dx$ com $o = x/7$ e $do=\dfrac{dx}{7}$. 
temos aqui então $v=\int e^o7do$, ou seja, $v=7e^{x/7}$ 

Substituindo na fórmula:

$\int xe^xdx = 7xe^{x/7} - \int e^{x/7} \cdot 1dx$

$\int xe^xdx = 7xe^{x/7} - 7e^{x/7}$

Simplificando:

$\int xe^xdx = 7e^{x/7}(x-1) + c$