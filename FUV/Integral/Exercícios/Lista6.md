# Lista 6

## Ex. 2 

a) $\int_0^1x^7+3xdx$

$\int x^7+3xdx=\dfrac{x^8}{8}+\dfrac{3x^2}{2}$

$\int_0^1x^7+3xdx=\dfrac{1^8}{8}+\dfrac{3\cdot 1^2}{2} -\dfrac{0^8}{8}+\dfrac{3\cdot 0^2}{2} = \dfrac{1}{8} + \dfrac{3}{2} - 0 = \dfrac{1}{8} + \dfrac{12}{8} = \boxed{\dfrac{13}{8}}$

## Ex 3 

a) $\int cos(3x)dx$;
Considere $u = 3x$; Então $du = 3dx; dx = \frac{du}3$

$\int cos(u)du = sen(u) = \boxed{sen(3x) + c}$

b) $\int x(4+x^2)^{10}dx$;
Considere $u = 4+x^2$, $du=xdx$

$\int u^{10}du = \int \dfrac{u^{11}}{11} = $