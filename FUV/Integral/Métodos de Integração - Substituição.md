# Método da substituição

A ideia por trás desse método é encontrar uma forma de integrar qualquer função complexa, transformando-a em uma função mais simples e que pode ser integrada usando uma tabela.

Para isso é necessário identificar a parte que será substituida pela função $u(x)$.

## Exemplo 1:
$\int e^{4x} dx$

Para esse caso, vamos assumir $u(x) = 4x$.
Assim, teremos $du=4dx$, e portanto, $dx=\frac{du}{4}$

Então, reescrevemos nossa integral:

$\int e^u\dfrac{du}{4} = \dfrac{1}{4}\int e^udu$

Agora, sabemos que a integral da exponencial é a própria exponencial. Então, temos:

$\dfrac{1}{4}\int e^udu = \dfrac{1}{4}e^u$

Por último, voltamos a substituir o valor de $u$.

$\dfrac{1}{4}\int e^udu = \dfrac{1}{4}e^{4x}+c$

## Exemplo 2:
$\int cos(3x) dx$

Nesse exemplo, $u=3x$. Ou seja, $du=3dx \rightarrow dx=\frac{du}{3}$ 
Então:

$\int cos(u)\dfrac{du}{3} = \dfrac{1}{3}\int cos(u)du$

$\dfrac{1}{3}\int cos(u)du = \dfrac{1}{3}sen(u)$

Substituindo $u$ por $3x$

$\dfrac{1}{3}\int cos(u)du = \dfrac{1}{3}sen(3x)+c$

## Exemplo 3

$\int(1-cos(4x)+sen(\frac{x}{7}))dx$

Aqui temos que aplicar uma propriedade das integrais que diz que a integral da soma é a soma das integrais.  
Portanto:
$\int(1-cos(4x)+sen(\frac{x}{7}))dx = \int dx - \int cos(4x)dx + \int sen(\frac{x}{7})dx$

Basta calcular a integral de cada uma das partes.

**Parte 1**  
$\int dx = x$

**Parte 2**  
$-\int cos(4x)dx$

Tome $u=4x$

$-\int cos(u)\frac{du}{4} = -\frac{1}{4}\int cos(u)du = -\frac{sen(u)}{4}$

$\therefore -\int cos(4x)dx = -\dfrac{sen(4x)}{4}$

**Parte 3**  
$sen(\frac{x}{7})$

Tome $u=\frac{x}{7}$

$\int sen(u)7du = 7\int sen(u)du = -7cos(u)$

$\therefore sen(\frac{x}{7}) = -7cos(\dfrac{x}{7})$

## $\int(1-cos(4x)+sen(\frac{x}{7}))dx = x-\dfrac{sen(4x)}{4}-7cos(\dfrac{x}{7})$

# Exemplo 4
