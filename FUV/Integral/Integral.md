# Integral

 Integral ou Antiderivada($F(x)$) de uma função, é tal que, em um intervalo fechado $[a,b]$, temos $f(x)$ contínua e $F'(x) = f(x)$.

 # Integral Indefinida

 Uma integral indefinida de uma função possui a seguinte notação:

 $$\int f(x)dx = F(x) + c$$
 Sendo $c \in R$.

 # Integral Definida

A integral definida possui a seguinte notação:

 $$\int_0^2 f(x)dx$$

 Para entender a integral definida, precisamos entender o que estamos procurando com integral.
 Queremos obter a área sob um gráfico, basicamente, somaremos retângulos aproximando-os do valor de f(x). Quanto mais retângulos tivermos, maior será a precisão da nossa soma.

 Considere então, a função $f(x)$ e o intervalo fechado $[a,b]$.
 Para esse intervalo, somaremos $n$ retangulos usando para a aproximação pela direita, ou seja, temos o seguinte somatório:

 $$\sum_{i=0}^n f(x_i^\ast)\Delta x$$
 $$\Delta x = \frac{b-a}{n}*n$$

 Assim, como queremos que nosso valor de $\Delta x$ tenda a zero, temos:

 $$\int_a^bf(x)dx \approx \lim\limits_{\Delta x\rightarrow 0}\sum_{i=1}^n f(x_i^\ast)\Delta x$$

 E pelas propriedades do somatório, teremos portanto:

$$\int_a^bf(x)dx \approx \lim\limits_{\Delta x\rightarrow 0}\Delta x\sum_{i=1}^n f(x_i^\ast)$$

Essa soma é chamada de "Soma de Riemann".

# Teorema fundamental do cálculo

O teorema fundamental do cálculo descreve a seguinte relação:

 $$\int_a^bf(x)dx = F(b) - F(a)$$

 Ou seja, não é necessário utilizar a soma de Riemann para obter a integral definida em um intervalo. 

 > Exemplo: 
 > Dado $f(x)=x^2$, calcule a área sob o gráfico no intervalo [1,3]:  
 > Primeiro, integramos $f(x)$: $\int x^2dx = \dfrac{x^3}{3} + c$  
 > $\int_1^3 x^2dx = F(3) - F(1) = \dfrac{3^3}{3}-\dfrac{1^3}{3}$  
 > $\int_1^3 x^2dx = \dfrac{27-1}{3} = \dfrac{26}{3} = 8,666...$ 

# Integrais importantes:

1. $\int dx = x+c$

2. $\int x^n = \dfrac{x^{n+1}}{n+1}$

3. $\int 3^x dx=\dfrac{3^x}{ln(3)}$

# Propriedades das Integrais($\int$)

1. $\int (f(x) \pm g(x))dx = \int f(x) dx \pm \int g(x) dx$

2. $\int (c\cdot f(x))dx = c\cdot \int f(x)dx$