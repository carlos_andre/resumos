# Área entre curvas

Considere $g(x)=\sqrt{x}$ e $f(x)=x^2$.

Queremos saber 

# Volume por revolução

## Em torno do eixo x(Revolução horizontal)
$V = \pi\int_a^b f(x)^2dx$

Caso seja entre curvas:
$V = \pi\int_a^b|f(x)^2-g(x)^2|dx$

## Em torno do eixo y(Revolução Vertical)
$V = 2\pi\int_a^b x|f(x)|dx$

Caso seja entre curvas:
$V = 2\pi\int_a^b x|f(x)-g(x)|dx$

# Comprimento do Arco
$c=\int_a^b\sqrt{1+\big( \dfrac{d}{dx}f(x) \big)^2}dx$

