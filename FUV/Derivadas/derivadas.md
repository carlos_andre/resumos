# Derivadas

## Conceitos envolvidos

### Reta secante

O conceito de reta secante está intimamente ligado ao conceito de derivada. Podemos chamar de reta secante a reta que corta a curva $f(x)$ em dois pontos: $(x_1,f(x_1))$ e $(x_2,f(x_2))$:

![Reta Secante](secante.png)

### Reta Tangente

A reta tangente toca a curva em apenas um ponto. Para calcular a reta tangente $y$ no ponto $(a,f(a))$, devemos usar a seguinte função:

$$y = \bigg(\lim\limits_{x\rightarrow a}\frac{f(x)-f(a)}{x-a}\bigg)\cdot(x-a)+ f(a)$$

![Reta Tangente](tangente.png)
### Relação reta tangente e inclinação da reta no ponto $x$

A reta tangente é representada por uma função de 1º grau, ou seja, tem o formato $f(x)=ax+b$, onde $a$ é a inclinação da reta. Na reta tangente, o valor de a é dado por $\lim\limits_{x\rightarrow a}\frac{f(x)-f(a)}{x-a}$. 

## Definição

A derivada representa a inclinação da função em um ponto x e pode ser dada pela inclinação da reta tangente naquele ponto.

Assim, a derivada de $f(x)$ é dada por:

$$\lim\limits_{x\rightarrow a}\frac{f(x)-f(a)}{x-a}$$

Ou, tomando $h = x-a$ 

$$\lim\limits_{x\rightarrow a}\frac{f(a+h)-f(a)}{h}$$

> **Exemplo: $f(x)=x^2$**
> $\lim\limits_{x\rightarrow a}\frac{f(x)-f(a)}{x-a} = \lim\limits_{x\rightarrow a}\frac{x^2-a^2}{x-a} = \frac{(x+a)(x-a)}{x-a}=x+a$  
>Dado que $x\rightarrow a$, temos que $\lim\limits_{x\rightarrow a}\frac{f(x)-f(a)}{x-a} = 2a$.  
Portanto, para $f(x) =x^2$, temos que $f'(x)=2x$.

## Propriedades de derivadas

**Teorema:** Sejam f e g deriváveis em x=a e C uma constante. Então:

1. $(c\cdot f(x))' = c\cdot f'(x)$
2. $f(x)+g(x)=f'(x)+g'(x)$
3. $(f(x)\cdot g(x))'=f'(x)\cdot g(x)+f(x)\cdot g'(x)$
4. Se $g(x)\ne 0 \rightarrow\dfrac{1}{g(x)}=\dfrac{g'(x)}{g^2(x)}$

5. Se $g(x)\ne 0 \rightarrow\dfrac{f(x)}{g(x)}=\dfrac{f'(x)\cdot g(x)+f(x)\cdot g'(x)}{g^2(x)}$

# Tabela de Derivadas

$f(x)$ | $f'(x)$
---|---
$c$|0
$x$|1
$x^2$|$2x$
$x^n$|$n\cdot x^{(n-1)}$
$\dfrac{1}{x}$|$\dfrac{1}{x^2}$
$\dfrac{1}{x^n}$|$\dfrac{n}{x^{n+1}}$
$\sqrt{x}$|$\dfrac{1}{2\sqrt{x}}$
$\sqrt[n]{x}$|$\dfrac{1}{n\cdot\sqrt[n]{x^{n-1}}}$
$sen(x)$|$cos(x)$
$cos(x)$|$-sen(x)$
$tan(x)$|$1+tan^2(x)$

# Regra da Cadeia

Aqui temos que $(f\cdot g)'(x) = f'(g(x))\cdot g'(x).$

> Exemplo: $sen(\sqrt{x})$.  
>Tomando $g(x)=\sqrt{x}$ e $f(x)=sen(x)$, temos que $f(g(x))=sen(\sqrt{x})$.  
Aplicando aqui a regra da cadeia, temos $f'(g(x))\cdot g'(x) = cos(\sqrt{x})\cdot g'(x)=\dfrac{cos(\sqrt{x})\cdot 1}{2\sqrt{x}}$


# Teorema: Derivada da Inversa
    pendente

# Derivada dupla 

Define-se uma derivada dupla como $\dfrac{d^2y}{dx^2}$, e para obtê-la, basta derivar a derivada de $f(x)$.

Também pode-se escrever a derivada dupla como $\dfrac{d(\frac{dy}{dx})}{dx}$.

Uma outra notação possível é $f''(x)$

> Exemplo: $f(x)=x^2 => f'(x)=2x => f''(x)=2$ 

# A n-ésima derivada

Seguindo a mesma lógica, temos que a n-ésima derivada de f(x), pode ser dada como:

$$\dfrac{d(\frac{d^{n-1}y}{dx^{n-1}})}{dx}$$


# Aproximação linear
A ideia aqui é avaliar e obter valores próximos para um ponto f(x), através da aproximação da reta tangente.

> Exemplo: Para determinar a aproximação para $\sqrt{15}$  
> Primeiro calculamos a reta tangente para a função que melhor se encaixa. Nesse caso, usaremos $f(x)=\sqrt(x)$. Descobriremos $f(16)$ e usaremos esse ponto na reta da tangente para calcular, aproximadamente, o valor de $\sqrt{15}$.  
> $f(x)=\sqrt{x} => f(16)=\sqrt{16}=4$  
> $f'(x) = \dfrac{1}{2\sqrt{x}}$  
> $y = f'(x_0)\cdot (x-x_0)+f(x_0)$  
> Então, para o ponto $(16,4):$  
> $y = f'(16)\cdot (x-16)+f(16)$  
> $y = \dfrac{1}{8}\cdot (x-16)+4$  
> $y = \dfrac{1}{8}\cdot -1+4$  
> $f(15) \approx 3,875$

