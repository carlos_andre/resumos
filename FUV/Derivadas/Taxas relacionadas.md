# Taxas relacionadas

## Definição
 A ideia aqui é estudar como uma determinada taxa se comporta em relação a uma outra, usando técnicas de derivação e relacionando as duas taxas através de uma função.

## Passos

Existem alguns passos que podem ser seguidos para obter o resultado. Os passos são os descritos abaixo:

1. Encontrar as variáveis.
2. Relacionar as variáveis através de uma função.
3. Derivar ambos os lados.
4. Substituir valores conhecidos.
5. Isolar o termo procurado.

> Dica: Para facilitar o processo de teste, o ideal é utilizar a notação de Leibniz. $\dfrac{dx}{dy}$

 