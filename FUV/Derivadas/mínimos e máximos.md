# Máximos e Mínimos

A ideia aqui é encontrar o máximo e o mínimo de uma função através de derivadas. Para fazer isso temos que observar alguns critérios.  
Sabemos que a Derivada trata especificamente da inclinação da reta tangente em um determinado ponto, em um ponto máximo ou mínimo, espera-se que o valor dessa derivada seja zero, se em um intervalo $[a,b]$:  
a.
O intervalor $(a,b)$ seja diferenciável;  
b. 

# Descobrindo os máximos e mínimos

Para descobrir o máximo e mínimo de uma função, basta fazer uma análise cuidadosa de sua derivada e de seus sinais.
Supondo que procuremos os máximos e mínimos de $f(x)$:
Inicialmente, derivamos a função $f(x)$, e igualamos essa derivada a zero. Dessa forma, descobriremos um ponto da reta com inclinação nula(Ou seja, $f'(x) = 0$).  
Depois disso, analisamos o sinal de $f'(x)$ nas regiões onde $f'(x)=0$. Tal análise nos ajuda a entender se a função é crescente ou decrescente.
Se uma função é crescente antes de f'(x)=0 e descrescente depois disso, temos que esse é um ponto de máximo local. 
Caso ocorra o contrário, temos que esse é um ponto mínimo local.
E, caso o sinal de f'(x) seja o mesmo, temos um ponto de inflexão.