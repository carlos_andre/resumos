# A Regra de L'Hopital 

A Regra de L'Hopital é comumente usada para calcular limites que resultam em algum tipo de indeterminação.

Para isso, ela utiliza-se do seguinte artifício:

Considere $f$ e $g$, funções deriváveis em um intervalo $I$.
Com $g'(x)\neq 0$, $\forall x \in I$.

Se $\lim\limits_{x\rightarrow p}f(x) = 0 = \lim\limits_{x\rightarrow p}g(x)$ ou $\lim\limits_{x\rightarrow p}f(x) = \infty = \lim\limits_{x\rightarrow p}g(x)$

Assim, temos $\lim\limits_{x\rightarrow p}\dfrac{f(x)}{g(x)}$ = $\lim\limits_{x\rightarrow p}\dfrac{f'(x)}{g'(x)}$

# Quando utilizar

A regra de L'hôpital, deve ser usada para calcular limites de alguns tipos de indeterminação:

Indeterminações: $\dfrac{0}{0},\dfrac{+\infty}{+\infty},\dfrac{-\infty}{-\infty}$

Alguns exemplos:

- $\lim\limits_{x\rightarrow 0}\dfrac{sen(x)}{x} =\bigg[\dfrac{0}{0}\bigg]=\lim\limits_{x\rightarrow 0}\dfrac{(sen(x))'}{x'} = \lim\limits_{x\rightarrow 0}\dfrac{cos(x)}{1} = 1$

- $\lim\limits_{x\rightarrow 0}\dfrac{tg(x)}{x} =\bigg[\dfrac{0}{0}\bigg]= \lim\limits_{x\rightarrow 0}\dfrac{(tg(x))'}{x'} = \lim\limits_{x\rightarrow 0}\dfrac{sec^2(x)}{1} = 1$

- $\lim\limits_{x\rightarrow 1}\dfrac{x^2-1}{x-1} =\bigg[\dfrac{0}{0}\bigg]= \lim\limits_{x\rightarrow 1}\dfrac{(x^2-1)'}{(x-1)'} = \lim\limits_{x\rightarrow 1}\dfrac{2x}{1} = 2$

- $\lim\limits_{x\rightarrow \infty}\dfrac{x}{e^x} =\bigg[\dfrac{\infty}{\infty}\bigg]= \lim\limits_{x\rightarrow \infty}\dfrac{(x)'}{(e^x)'} = \lim\limits_{x\rightarrow \infty}\dfrac{1}{e^x} = 0$

